api = 2
core = 7.x

defaults[projects][subdir] = contrib

projects[oauth2_client][version] = 2.0

projects[hybridauth][version] = 2.16

libraries[hybridauth][type] = library
libraries[hybridauth][directory_name] = hybridauth
libraries[hybridauth][download][type] = get
libraries[hybridauth][download][url] = https://codeload.github.com/hybridauth/hybridauth/tar.gz/refs/tags/v2.11.0

libraries[hybridauth-drupaloauth2][type] = library
libraries[hybridauth-drupaloauth2][directory_name] = hybridauth-drupaloauth2
libraries[hybridauth-drupaloauth2][download][type] = git
libraries[hybridauth-drupaloauth2][download][url] = https://gitlab.com/dashohoxha-drupal/libraries/hybridauth-drupaloauth2/


;---------------------------------------
; Dependencies of submodule oauth2_user.
;---------------------------------------

projects[http_client] = 2.x-dev

projects[wsclient][version] = 1.0
projects[wsclient][patch][] = https://drupal.org/files/wsclient-1285310-http_basic_authentication-14.patch
projects[wsclient][patch][] = https://drupal.org/files/issues/wsclient-2138617-oauth2_support.patch
